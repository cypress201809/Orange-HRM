import Login from "../pages/LoginPage.js";
import Dashboard from "../pages/DashboardPage.js";

describe('OrangeHRM Dashboard Page', () => {

    beforeEach(() => {
        cy.visit("https://opensource-demo.orangehrmlive.com/")
        cy.fixture('orangehrm').then((data)=>{
            const ln=new Login();
            ln.setUserName(data.username)
            ln.setPassword(data.password)
            ln.clickSubmit();
            ln.verifyLogin();
        })
      });

    it("should display the correct title", ()=>{
        cy.title().should('include','Orange')
        .and('eq', "OrangeHRM")
        .and('contain',"HRM")
    })

    it("should open admin menu", () => {
        // cy.contains("Admin").click()
        cy.get("li.oxd-main-menu-item-wrapper")
            .eq(0)
            .should('have.text', 'Admin')
            .click()
    })

    it("Add user", () => {
        cy.contains("Admin").click()
        cy.get(".orangehrm-header-container > button").click()
    })

})