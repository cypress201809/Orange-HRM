import EcommerceHomePage from "../pages/EcommerceHomePage.js";

const home = new EcommerceHomePage()
  
 describe("testing home page", () => {
   it("should visit home page and search", () => {
     home.visit()
     home.searchInput("iphone")
     home.getSearchButton().click()
   })
  })
 