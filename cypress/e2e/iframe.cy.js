describe("handling frames",()=>{
    it('handline iframe in cypress',()=>{

        cy.visit("http://the-internet.herokuapp.com/iframe");

        const iframe=cy.get("#mce_0_ifr")
            .its('0.contentDocument.body') //1st document under frame
            .should('be.visible')
            .then(cy.wrap); //wrap the document frame

            iframe.clear() //clear default text
            iframe.type("Welcome {selectAll}"); //clear default values and write 
            cy.get("[aria-label='Bold']").click(); //bold the text
    })
})