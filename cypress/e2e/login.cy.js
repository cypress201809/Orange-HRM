import Login from "../pages/LoginPage.js";

describe('Test Scenario: Test Functionality of Login Page', () => {

    //General approach
    it('LoginTest', () => {
        cy.visit("https://opensource-demo.orangehrmlive.com/")
        cy.get("input[placeholder='Username']").type("Admin")
        cy.get("input[placeholder='Password']").type("admin123")
        cy.get("button[type='submit']").click()
        cy.get('.oxd-topbar-header-breadcrumb > .oxd-text').should('have.text', 'Dashboard');
    })

       //using pom
    it('LoginTest', () => {
        cy.visit("https://opensource-demo.orangehrmlive.com/")
        
        const ln=new Login();
        ln.setUserName("Admin")
        ln.setPassword("admin123")
        ln.clickSubmit();
        ln.verifyLogin();
    })

       //using pom with fixture
       it.only('LoginTest', () => {
        cy.visit("https://opensource-demo.orangehrmlive.com/")

        cy.url().should('include','orangehrmlive.com')
       .and('eq','https://opensource-demo.orangehrmlive.com/web/index.php/auth/login')
       .and('contain','orangehrm')
       .and('not.contain','greenhrm')
        
        cy.fixture('orangehrm').then((data)=>{
            const ln=new Login();
            ln.setUserName(data.username)
            ln.setPassword(data.password)
            ln.clickSubmit();
            ln.verifyLogin();
        })
    })
 })
