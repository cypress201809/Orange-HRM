class DashboardPage
{
    setUserName(username)
    {
        cy.get("input[placeholder='Username']").type(username);
    }
}

export default DashboardPage;